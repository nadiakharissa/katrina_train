import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://test-katrina.icore.studio/')

WebUI.setText(findTestObject('Object Repository/Page_Login  Katrina Reservation/input_MEMBER LOGIN_user_name'), 'kharissa_train')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Login  Katrina Reservation/input_MEMBER LOGIN_user_password'), 
    'YFZfyx+NhJE=')

WebUI.click(findTestObject('Object Repository/Page_Login  Katrina Reservation/button_Sign In'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Page_Dashboard  Katrina Reservation/span_Trains'))

WebUI.click(findTestObject('Page_Dashboard  Katrina Reservation/Page_Train Search  Katrina Reservation/input_From_station_src_name'))

WebUI.setText(findTestObject('Page_Dashboard  Katrina Reservation/Page_Train Search  Katrina Reservation/input_From_station_src_name'), 
    'gmr')

WebUI.click(findTestObject('Page_Dashboard  Katrina Reservation/Page_Train Search  Katrina Reservation/a_Gambir (GMR) Jakarta'))

WebUI.click(findTestObject('Page_Dashboard  Katrina Reservation/Page_Train Search  Katrina Reservation/input_To_station_dest_name'))

WebUI.setText(findTestObject('Object Repository/Page_Dashboard  Katrina Reservation/Page_Train Search  Katrina Reservation/input_To_station_dest_name'), 
    'bd')

WebUI.click(findTestObject('Page_Dashboard  Katrina Reservation/Page_Train Search  Katrina Reservation/a_Bandung (BD) Bandung'))

WebUI.click(findTestObject('Page_Dashboard  Katrina Reservation/Page_Train Search  Katrina Reservation/input_Depart_departure_date_time'))

WebUI.delay(10)

WebUI.click(findTestObject('Page_Dashboard  Katrina Reservation/Page_Train Search  Katrina Reservation/div_January2020'))

WebUI.click(findTestObject('Object Repository/Page_Train Search  Katrina Reservation/a_14'))

WebUI.click(findTestObject('Object Repository/Page_Train Search  Katrina Reservation/button_Search'))

WebUI.delay(20)

WebUI.click(findTestObject('Object Repository/Page_Train Search Result  Katrina Reservation/button_Select'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Page_Train Search Result  Katrina Reservation/button_BOOK NOW'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Page_Input Passenger(s)  Katrina Reservation/button_FILL CONTACT DETAILS'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Page_Input Passenger(s)  Katrina Reservation/button_Select'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Page_Input Passenger(s)  Katrina Reservation/label_Im Traveller'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Page_Input Passenger(s)  Katrina Reservation/button_BOOK TO ORDER'))

WebUI.click(findTestObject('Object Repository/Page_Confirmation  Katrina Reservation/btn_BOOK NOW'))

WebUI.click(findTestObject('Object Repository/Page_Booking Detail  Katrina Reservation/span_3A'))

WebUI.switchToWindowIndex(1, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_Booking Detail  Katrina Reservation/button_Select'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Page_Booking Detail  Katrina Reservation/button_Confirm Seat'))

WebUI.closeBrowser()

